import { Component, OnInit, Input } from '@angular/core';
import { Pet } from 'src/models/pet.model';

import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { PetService } from '../services/pet/pet.service'
import { User } from 'src/models/user.model';
import { AppComponent } from '../core/app/app.component';
import { AppointmentsComponent } from '../appointments/appointments.component';
import { Appointment } from 'src/models/appointment.model';
import { AppointmentService } from '../services/appointment/appointment.service';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.scss']
})
export class PetDetailComponent implements OnInit {
  pet: Pet;
  loggedInUser: User = AppComponent.loggedInUser;
  newDate: Date = new Date();

  constructor(
    private route: ActivatedRoute, 
    private petService: PetService, 
    private location: Location,
    private appointmentService: AppointmentService
    ) { }

  ngOnInit() {
    this.getPet();
  }

  getPet(): void {
    const id = this.route.snapshot.paramMap.get('id');

    console.log(id)
    this.petService.getPet(id).subscribe(pet => {
      this.pet = pet
    })
  }

  purchasePet(){
    const data: Appointment = {
      _id: null,
      date: this.newDate,
      userId: this.loggedInUser._id,
      petId: this.pet._id
    }
    this.appointmentService.createAppointment(data)
  }

  deletePet(){
    const id = this.route.snapshot.paramMap.get('id');

    this.petService.deletePet(id)
  }
}
