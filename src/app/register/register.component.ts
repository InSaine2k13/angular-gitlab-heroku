import { Component, OnInit } from '@angular/core'
import { User } from 'src/models/user.model'
import { Observable } from 'rxjs'
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-register-registration',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  newUser: Observable<any>;
  newUsername: string;
  newPassword: string;

  constructor(private userService: UserService) {}
  
  createUser(){
    const data: User = {
      _id: null,
      username: this.newUsername,
      password: this.newPassword,
      isEmployee: true
    }
      //this.users = this.userService.getUsers()
      this.userService.createUser(data)
  }



  ngOnInit() {}
}
