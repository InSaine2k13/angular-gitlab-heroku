import { Component, OnInit } from '@angular/core'
import { UseCase } from '../../../models/usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registreren',
      description: 'Hiermee kan een nieuwe gebruiker zich registreren.',
      scenario: [
        'Bezoeker klikt op de knop om te registreren',
        'De registratie pagina wordt geopend en de gebruiker vult zijn gegevens in',
        'Gebruiker klikt op register knop'
      ],
      actor: 'Bezoeker',
      precondition: '-',
      postcondition: 'Nieuwe gebruiker is toegevoegd aan de database'
    },
    {
      id: 'UC-02',
      name: 'Inloggen',
      description: 'Hiermee kan een bestaande gebruiker inloggen.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Gebruiker',
      precondition: 'Gebruiker is geregistreerd',
      postcondition: 'De gebruiker is ingelogd'
    },
    {
      id: 'UC-03',
      name: 'Alle dieren ophalen',
      description: 'Haalt alle dieren op',
      scenario: [
        'De applicatie toont alle dieren',
        'Een bezoeker of gebruiker selecteert een dier',
        'Er wordt informatie getoond over het geselecteerde dier'
      ],
      actor: 'Gebruiker',
      precondition: 'Een pagina waar alle dieren worden weergeven wordt geladen',
      postcondition: 'Alle dieren worden opgehaald en weergeven op beeld'
    },
    {
      id: 'UC-04',
      name: 'Toevoegen van een dier',
      description: 'Een medewerker kan zo een nieuw dier toevoegen',
      scenario: [
        'Medewerker navigeert naar pagina voor het aanmaken van dieren',
        'Medewerker vult de gegevens voor het dier in',
        'Medewerker klikt op opslaan knop'
      ],
      actor: 'Medewerker',
      precondition: 'Gebruiker is ingelogd en is een medewerker',
      postcondition: 'Het dier is aangemaakt en opgeslagen op de database'
    },
    {
      id: 'UC-05',
      name: 'Verwijderen van een dier',
      description: 'Een medewerker kan zo een dier verwijderen',
      scenario: [
        'Medewerker navigeert naar pagina die alle dieren toont',
        'Medewerker klikt op het prullenbak icoontje die alleen voor medewerkers zichtbaar is'
      ],
      actor: 'Medewerker',
      precondition: 'Gebruiker is ingelogd en is een medewerker',
      postcondition: 'Dier wordt verwijdert van de database'
    },
    {
      id: 'UC-06',
      name: 'Aanpassen van een dier',
      description: 'Past een dier aan',
      scenario: [
        'Medewerker navigeert naar pagina die alle dieren toont',
        'Medewerker klikt op het dier dat die wilt aanpassen',
        'Detail pagina van het dier wordt geopend',
        'Op de detail pagina kan de medewerker op een pen icoontje drukken die alleen voor medewerkers zichtbaar is',
        'Vervolgens worden de detail velden aanpasbaar en kan de medewerker aanpassingen brengen',
        'Na gewenste wijzigingen te maken klikt de gebruiker op opslaan'
      ],
      actor: 'Medewerker',
      precondition: 'Gebruiker is ingelogd en is een medewerker',
      postcondition: 'Het dier is in de database aangepast'
    },
    {
      id: 'UC-07',
      name: 'Afspraak maken',
      description: 'Aanmaken van een afspraak om dier te kopen',
      scenario: [
        'Gebruiker navigeert naar pagina die alle dieren toont',
        'Hij klikt op het dier dat hij wilt kopen en ophalen',
        'Vervolgens opent de detail pagina van het dier en hier klikt hij op het winkelwagen icoontje',
        'Nu krijgt hij de optie om een datum te selecteren',
      ],
      actor: 'Gebruiker',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Afspraak wordt toegevoegd aan de database'
    },
    {
      id: 'UC-08',
      name: 'Afronden van een afspraak',
      description: 'Een afspraak wordt afgerond wanneer een gebruiker het dier ophaalt',
      scenario: [
        'Een medewerker gaat naar de lijst met afspraken',
        'Hij navigeert naar de afspraak van de gebruiker die het dier is komen ophalen',
        'Vervolgens klikt hij op het vinkje bij de item in de lijst',
      ],
      actor: 'Medewerker',
      precondition: 'Gebruiker is ingelogd en is een medewerker',
      postcondition: 'De afspraak en het dier worden verwijdert uit de database'
    },
    {
      id: 'UC-09',
      name: 'Een gebruiker medewerker rechten geven',
      description: 'Een medewerker geeft een andere gebruiker medewerker rechten',
      scenario: [
        'Een medewerker gaat naar de pagina met een lijst met gebruikers',
        'Vervolgens klikt de medewerker op een sterretje bij de juiste gebruiker',
      ],
      actor: 'Medewerker',
      precondition: 'Gebruiker is ingelogd en is een medewerker',
      postcondition: 'Boolean isEmployee verandert naar true bij een gebruiker'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
