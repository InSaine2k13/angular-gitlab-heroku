import { Component, OnInit } from '@angular/core'
import { User } from 'src/models/user.model';
import {AppComponent } from 'src/app/core/app/app.component'
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userLogin: User = { _id: '0', username: '', password: '', isEmployee: false };

  loginUsername: string = '';
  loginPassword: string = '';

  constructor(private userService: UserService) {}

  ngOnInit() {
    console.log(AppComponent.loggedInUser.username)
  }

  login() {
    this.userService.getUserForLogin(this.loginUsername).subscribe(userLogin => {
      this.userLogin = userLogin
      console.log(this.loginUsername)
      console.log(this.userLogin.username)
      if(this.userLogin.username === this.loginUsername && this.userLogin.password === this.loginPassword){
        this.userService.getUserForLogin(this.loginUsername).subscribe(userLogin => { AppComponent.loggedInUser = userLogin })
        AppComponent.loggedInUser = this.userLogin;

        console.log('Logged in as employee')
        console.log(AppComponent.loggedInUser.username)
        console.log(AppComponent.loggedInUser.isEmployee)
      }
    })

  }
}
