import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/models/animal.model';
import { User } from 'src/models/user.model';
import { AppComponent } from '../core/app/app.component';
import { AnimalService } from '../services/animal/animal.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.scss']
})
export class AnimalsComponent implements OnInit {
  animals: Animal[];
  loggedInUser: User = AppComponent.loggedInUser;

  constructor(private animalService: AnimalService) {}

  ngOnInit() {
    this.getAnimals();
  }

  getAnimals() {
    this.animalService.getAnimals().subscribe(animals => (this.animals = animals));
  }
}