import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnimalService } from '../services/animal/animal.service';
import { AppComponent } from '../core/app/app.component';
import { User } from 'src/models/user.model';
import { Animal } from 'src/models/animal.model';
import { Location } from '@angular/common'

@Component({
  selector: 'app-animal-detail',
  templateUrl: './animal-detail.component.html',
  styleUrls: ['./animal-detail.component.scss']
})
export class AnimalDetailComponent implements OnInit {
  animal: Animal;
  loggedInUser: User = AppComponent.loggedInUser;

  constructor(
    private route: ActivatedRoute, 
    private animalService: AnimalService, 
    private location: Location
    ) { }

  ngOnInit() {
    this.getAnimal();
  }

  getAnimal(): void {
    // remove the + when id is a string
    const id = +this.route.snapshot.paramMap.get('id');

    this.animalService.getAnimal(id).subscribe(animal => (this.animal = animal));
  }
}
