import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppointmentService } from '../services/appointment/appointment.service';
import { User } from 'src/models/user.model';
import { AppComponent } from '../core/app/app.component';
import { Appointment } from 'src/models/appointment.model';
import { Location } from '@angular/common'
import { PetService } from '../services/pet/pet.service';


@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.scss']
})

export class AppointmentDetailComponent implements OnInit {
  appointment: Appointment;
  loggedInUser: User = AppComponent.loggedInUser;

  constructor(
    private route: ActivatedRoute, 
    private animalService: AppointmentService, 
    private location: Location,
    private petService: PetService,
    private appointmentService: AppointmentService
    ) { }

  ngOnInit() {
    this.getAppointment();
  }

  getAppointment(): void {
    // remove the + when id is a string
    const id = this.route.snapshot.paramMap.get('id');

    this.animalService.getAppointment(id).subscribe(appointment => (this.appointment = appointment));
  }

  appointmentDone(){
    this.appointmentService.finishAppointment(this.appointment._id, this.appointment.petId)
  }

}
