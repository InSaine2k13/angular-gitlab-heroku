import { Component, OnInit } from '@angular/core'
import { UserService } from '../services/user/user.service'
import { User } from 'src/models/user.model';
import { AppComponent } from '../core/app/app.component';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: User[];
  loggedInUser = AppComponent.loggedInUser;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(users => (this.users = users));
  }
}
