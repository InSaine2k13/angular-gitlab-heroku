import { Component } from '@angular/core'
import { User } from 'src/models/user.model'
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  static loggedInUser: User = {_id: '1', username: 'loggedIn', password: 'loggedIn', isEmployee: true} //null;
  title = 'Petstore Hsaine'
  static readonly ROOT_URL = 'http://localhost:3000'
  posts: any;
  

  constructor(private http: HttpClient) {}

  public get(urlAddition: string){
    return this.http.get(AppComponent.ROOT_URL + urlAddition)
  }
}
