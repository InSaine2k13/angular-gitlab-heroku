import { Animal } from 'src/models/animal.model'

export const ANIMALS: Animal[] = [
  { id: 1, species: 'dogs', race: 'pitbull', primaryColor: 'grey', secondaryColor:'black', isWarmBlooded: true},
  { id: 2, species: 'cats', race: 'jaguar', primaryColor: 'orange', secondaryColor:'black', isWarmBlooded: true},
  { id: 3, species: 'elephant', race: 'indian elephant', primaryColor: 'grey', secondaryColor:'grey', isWarmBlooded: true},
]
