import { Injectable } from '@angular/core';
import { Animal } from 'src/models/animal.model';
import { ANIMALS } from './test-animals';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { AppComponent } from 'src/app/core/app/app.component';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  constructor() { }
  animals: any;

  getAnimals(): Observable<Animal[]> {
    //this.animals = AppComponent.http.get(AppComponent.ROOT_URL + '/animal')
    //return of(this.animals);
    return of(ANIMALS);
  }

  getAnimal(id: number): Observable<Animal>{
    return of(ANIMALS.find(animal => animal.id === id));
  }
}
