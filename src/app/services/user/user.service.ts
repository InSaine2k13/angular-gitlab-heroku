import { Injectable } from '@angular/core';
import { USERS } from './test-users';
import { Observable, of} from 'rxjs';
import { User } from 'src/models/user.model';
import { HttpClient } from '@angular/common/http'
import { AppComponent } from 'src/app/core/app/app.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  users: any;

  getUsers(): Observable<User[]> {
    this.users = this.http.get(AppComponent.ROOT_URL + '/user');
    return of(this.users);
  }

  getUser(_id: string): Observable<User>{
    return this.http.get<User>(AppComponent.ROOT_URL + '/user/' + _id);;
  }

  getUserForLogin(username: string): Observable<User>{
    return this.http.get<User>(AppComponent.ROOT_URL + '/user/login/' + username);;
  }

  createUser(data: User) {
    this.http.post(AppComponent.ROOT_URL + '/user', data).subscribe((data:any) => {
      console.log(data)
    })
  }

  deleteUser(id: string) {
    this.http.delete(AppComponent.ROOT_URL + '/user/' + id).subscribe((data:any) => {
      console.log(data)
    })
  }
}
