import { User } from 'src/models/user.model';

export const USERS: User[] = [
  { _id: '11', username: 'Bob', password: 'secretBob', isEmployee: true },
  { _id: '12', username: 'Joe', password: 'secretJoe', isEmployee: false },
  { _id: '13', username: 'Mark', password: 'secretMark', isEmployee: false }
]
