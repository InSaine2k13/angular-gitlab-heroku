import { Pet } from 'src/models/pet.model'
import { Animal } from 'src/models/animal.model'

const dog = new Animal(1, 'dogs', 'pitbull', 'Red', 'Black', true)

export const PETS: Pet[] = [
  { _id: '11', description: 'Big pitbull', price: 50, animalId: '1' },
  { _id: '12', description: 'Small pitbull', price: 50, animalId: '1' },
  { _id: '13', description: 'Cool pitbull', price: 50, animalId: '1' },
]
