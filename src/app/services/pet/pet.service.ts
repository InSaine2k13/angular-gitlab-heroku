import { Injectable } from '@angular/core';
import { Pet } from 'src/models/pet.model'
import { PETS } from './test-pets';
import { Observable, of} from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { AppComponent } from 'src/app/core/app/app.component';

@Injectable({
  providedIn: 'root'
})
export class PetService {
  pets: any;

  constructor(private http: HttpClient) { }

  getPets(): Observable<Pet[]> {
    this.pets = this.http.get(AppComponent.ROOT_URL + '/pet');
    return of(this.pets);
  }

  getPet(_id: string): Observable<Pet>{
    return this.http.get<Pet>(AppComponent.ROOT_URL + '/pet/' + _id);;
  }

  deletePet(id: string) {
    this.http.delete(AppComponent.ROOT_URL + '/pet/' + id).subscribe((data:any) => {
      console.log(data)
    })
  }

  createPet(data: Pet) {
    this.http.post(AppComponent.ROOT_URL + '/pet', data).subscribe((data:any) => {
      console.log(data)
    })
  }
}
