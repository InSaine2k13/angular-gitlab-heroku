import { Animal } from 'src/models/animal.model'
import { Appointment } from 'src/models/appointment.model'
import { User } from 'src/models/user.model';
import { Pet } from 'src/models/pet.model';

const date: Date = new Date();
const secondDate: Date = new Date(2018, 0O5, 0O5);
const dog = new Animal(1, 'dogs', 'pitbull', 'Red', 'Black', true)
const testPet = new Pet('11', 'Big pitbull', 0, '1')
const user = new User('1', 'testUser', 'testPassword', false)

export const APPOINTMENTS: Appointment[] = [
  { _id: '1', date: date, petId: '1', userId: '1'},
  { _id: '2', date: secondDate, petId: '1', userId: '1'},
  { _id: '3', date: date, petId: '1', userId: '1'},
]
