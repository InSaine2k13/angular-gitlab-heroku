import { Injectable } from '@angular/core';
import { APPOINTMENTS } from './test-appointments';
import { of, Observable } from 'rxjs';
import { Appointment } from 'src/models/appointment.model';
import { AppComponent } from 'src/app/core/app/app.component';
import { HttpClient } from '@angular/common/http';
import { PetService } from '../pet/pet.service';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  appointments: any;

  constructor(private http: HttpClient, private petService: PetService) { }

  getAppointments(): Observable<Appointment[]> {
    this.appointments = this.http.get(AppComponent.ROOT_URL + '/appointment');
    return of(this.appointments);
  }

  getAppointment(_id: string): Observable<Appointment>{
    return this.http.get<Appointment>(AppComponent.ROOT_URL + '/appointment/' + _id);;
  }

  finishAppointment(appointmentId: string, petId: string) {
    this.http.delete(AppComponent.ROOT_URL + '/appointment/' + appointmentId).subscribe((data:any) => {
      console.log(data)
      this.http.delete(AppComponent.ROOT_URL + '/pet/' + petId).subscribe((data:any) => {
        console.log(data)
      })
    })
  }

  deleteAppointment(id: string) {
    this.http.delete(AppComponent.ROOT_URL + '/appointment/' + id).subscribe((data:any) => {
      console.log(data)
    })
  }

  createAppointment(data: Appointment) {
    this.http.post(AppComponent.ROOT_URL + '/appointment', data).subscribe((data:any) => {
      console.log(data)
    })
  }

}
