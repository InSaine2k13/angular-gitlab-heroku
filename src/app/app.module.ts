import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { RegisterComponent } from './register/register.component'
import { LoginComponent } from './login/login.component'
import { PetsComponent } from './pets/pets.component'
import { FormsModule } from '@angular/forms';
import { PetDetailComponent } from './pet-detail/pet-detail.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { AnimalsComponent } from './animals/animals.component';
import { AnimalDetailComponent } from './animal-detail/animal-detail.component';
import { AppointmentDetailComponent } from './appointment-detail/appointment-detail.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    RegisterComponent,
    LoginComponent,
    UsecaseComponent,
    DashboardComponent,
    PetsComponent,
    PetDetailComponent,
    UsersComponent,
    UserDetailComponent,
    AnimalsComponent,
    AnimalDetailComponent,
    AppointmentDetailComponent,
    AppointmentsComponent,
  ],
  imports: [BrowserModule, RouterModule, NgbModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
