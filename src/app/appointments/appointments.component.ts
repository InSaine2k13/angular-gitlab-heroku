import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/models/appointment.model';
import { User } from 'src/models/user.model';
import { AppComponent } from '../core/app/app.component';
import { AppointmentService } from '../services/appointment/appointment.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  appointments: Appointment[];
  loggedInUser: User = AppComponent.loggedInUser;

  constructor(private appointmentService: AppointmentService) {}



  ngOnInit() {
    this.getAppointments();
  }

  getAppointments() {
    this.appointmentService.getAppointments().subscribe(appointments => (this.appointments = appointments));
  }
}

  
