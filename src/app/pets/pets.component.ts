import { Component, OnInit } from '@angular/core'
import { Pet } from 'src/models/pet.model'
import { PetService } from '../services/pet/pet.service'
import { AppComponent } from '../core/app/app.component';
import { User } from 'src/models/user.model';
import { Animal } from 'src/models/animal.model';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.scss']
})
export class PetsComponent implements OnInit {
  pets: Pet[];
  loggedInUser: User = AppComponent.loggedInUser;
  newPet: boolean = false;

  newDescription: string;
  newPrice: number;
  newAnimalId: string;
  

  constructor(private petService: PetService) {}

  ngOnInit() {
    this.getPets();
  }

  getPets() {
    this.petService.getPets().subscribe(pets => (this.pets = pets));
  }

  createPet(){
    const data: Pet = {
      _id: null,
      description: this.newDescription,
      price: this.newPrice,
      animalId: this.newAnimalId
    }
    this.petService.createPet(data)
    this.newPet = false;
  }

  startNewPet(){
    this.newPet = true;
  }
}
