import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Location } from '@angular/common'
import { UserService } from '../services/user/user.service'
import { User } from 'src/models/user.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  user: User;

  constructor(
    private route: ActivatedRoute, 
    private userService: UserService, 
    private location: Location
    ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    // remove the + when id is a string
    const id = this.route.snapshot.paramMap.get('id');

    console.log(id)
    this.userService.getUser(id).subscribe(user => {
      this.user = user
    })
  }

  deleteUser(){
    const id = this.route.snapshot.paramMap.get('id');

    this.userService.deleteUser(id)
  }
}
