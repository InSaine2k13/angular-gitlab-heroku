import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { RegisterComponent } from './register/register.component'
import { LoginComponent } from './login/login.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { UsersComponent } from './users/users.component'
import { PetDetailComponent } from './pet-detail/pet-detail.component'
import { UserDetailComponent } from './user-detail/user-detail.component'
import { AnimalsComponent } from './animals/animals.component'
import { AnimalDetailComponent } from './animal-detail/animal-detail.component'
import { AppointmentDetailComponent } from './appointment-detail/appointment-detail.component'
import { AppointmentsComponent } from './appointments/appointments.component'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent },
  { path: 'animals', component: AnimalsComponent },
  { path: 'appointments', component: AppointmentsComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'petDetail/:id', component: PetDetailComponent },
  { path: 'appointmentDetail/:id', component: AppointmentDetailComponent },
  { path: 'animalDetail/:id', component: AnimalDetailComponent },
  { path: 'userDetail/:id', component: UserDetailComponent },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
