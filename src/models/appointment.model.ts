export class Appointment {
  constructor(
    public _id: string,
    public date: Date,
    public userId: string,
    public petId: string
  ) {}
}
