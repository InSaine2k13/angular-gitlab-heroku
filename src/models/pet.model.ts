export class Pet {
  constructor(
    public _id: string,
    public description: string,
    public price: number,
    public animalId: string
  ) {}
}
