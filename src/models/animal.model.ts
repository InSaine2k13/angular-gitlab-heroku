export class Animal {
  constructor(
    public id: number,
    public species: string,
    public race: string,
    public primaryColor: string,
    public secondaryColor: string,
    public isWarmBlooded: boolean
  ) {}
}

